<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/verifikasi', function () {
    return view('verifikasi');
});

Route::get('/konfirmasi', function () {
    return view('konfirmasi');
});

Route::get('/forgot', function () {
    return view('forgot');
});


Route::get('/pembayaran', function () {
    return view('pembayaran');
});

Route::get('/beranda', function () {
    return view('beranda');
});

Route::get('/produk', function () {
    return view('produk');
});

Route::get('/domain', function () {
    return view('domain');
});

Route::get('/billing', function () {
    return view('billing');
});

Route::get('/billing_paid', function () {
    return view('billing_paid');
});

Route::get('/billing_unpaid', function () {
    return view('billing_unpaid');
});

Route::get('/billing_waiting', function () {
    return view('billing_waiting');
});

Route::get('/notifikasi', function () {
    return view('notifikasi');
});

Route::get('/profile', function () {
    return view('profile');
});

Route::get('/kontak', function () {
    return view('kontak');
});


Route::get('/info', function () {
    return view('info');
});

Route::get('/registry_lock', function () {
    return view('registry_lock');
});

Route::get('/layanan', function () {
    return view('layanan');
});

Route::get('/syarat_dan_ketentuan', function () {
    return view('syarat_dan_ketentuan');
});

Route::get('/faq', function () {
    return view('faq');
});

