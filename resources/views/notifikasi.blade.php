@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Notifikasi')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="grid grid-cols-1 gap-x-12 gap-y-5 md:grid-cols-2 md:px-32 px-5 pt-14 items-start">
        <div class="md:h-96 h-56 overflow-y-scroll md:pr-7 md:p-0 p-3 md:border-0 border-2 border-gray-400 md:rounded-none rounded-xl">
            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Profile :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Update data diri!') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>

            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Peringatan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Status Registran Lock!') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>

            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Tagihan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Invoicer #23534534') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>

            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Peringatan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Status Registran Lock!') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>

            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Peringatan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Status Registran Lock!') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>
            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Tagihan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Invoicer #23534534') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>
            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Tagihan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Invoicer #23534534') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>
            <div class="md:flex items-center justify-between bg-white rounded border border-gray-400 p-3.5 mb-3 hover:border-red-600 hover:shadow-md cursor-pointer">
                <div class="md:flex items-center">
                    <p class="text-red-600 font-bold text-md mr-2">{{ __('Peringatan :') }}</p>
                    <p class="text-gray-500 text-semibold text-md italic">{{ __('Status Registran Lock!') }}</p>
                </div>
                <div>
                    <p class="text-xs text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>

        </div>
        <div class="h-96 rounded-xl border-2 border-red-600 px-5 py-7 md:mt-0 mt-5">
            <div class="flex items-center justify-between mb-7">
                <div class="flex items-center">
                    <p class="text-red-600 font-bold text-md">{{ __('Detail Info :')}}</p>
                </div>
                <div>
                    <p class="text-sm text-gray-400">Rabu, 25 Agustus 2021</p>
                </div>
            </div>
            <p class="text-sm text-gray-500 font-bold mb-5">{{ __('Profil - Update data diri Anda') }}</p>
            <p class="text-sm text-gray-400 text-justify leading-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis ipsam at eum, sint dolorum debitis itaque doloribus excepturi harum a accusantium tempore rem, reiciendis et quidem similique? Autem, fugit sed?</p>
        </div>
    </div>
</main>

@include('layouts.footer')

