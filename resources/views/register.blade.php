<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="bg-no-repeat bg-cover bg-center relative" style="background-image: url({{ asset('bg-flag.jpg')}});">
            <div class="min-h-screen flex flex-col justify-center max-w-lg md:m-auto mx-5 mb-5 space-y-8">
                <form action="/verifikasi">
                    <div class="flex flex-col bg-white md:p-10 p-5 md:mt-10 mt-7 rounded-lg shadow-lg">

                        <div class="flex items-center justify-between md:mt-5">
                            <a href="/" class="btn btn-default w-50 text-xs"><i class="fa fa-arrow-left pr-2"></i>{{ __('Halaman Login') }}</a>
                            <img src="{{ asset('logo-color.png') }}" class="h-10">
                        </div>

                        <div class="grid auto-cols-max mt-10">
                            <h2 class="font-bold md:text-4xl text-2xl text-red-600">Pandi Registran Lock </h2>
                            <p class="font-bold text-2xl text-right">{{ __('Pendaftaran')}} </p>
                        </div>

                        <div class="space-y-2 md:flex items-center justify-between mt-5">
                            <label class="label md:w-1/3 w-full md:text-base text-sm">Nama Depan</label>
                            <input class="form-control md:w-2/3 w-full" type="text" id="first_name" name="first_name">
                        </div>
                        <div class="flex mt-1">
                            <div class="md:w-1/3"></div>
                            <div class="md:w-2/3 w-full">
                                <span class="text-gray-500 text-xs" id="msg_first_name">{{ __('')}}</span>
                            </div>
                        </div>
                        <div class="space-y-2 md:flex items-center justify-between mt-2">
                            <label class="label md:w-1/3 w-full md:text-base text-sm">Nama Belakang</label>
                            <input class="form-control md:w-2/3 w-full" type="text" id="last_name" name="last_name">
                        </div>
                        <div class="flex mt-1">
                            <div class="md:w-1/3"></div>
                            <div class="md:w-2/3 w-full">
                                <span class="text-gray-500 text-xs" id="msg_last_name">{{ __('')}}</span>
                            </div>
                        </div><div class="space-y-2 md:flex items-center justify-between mt-2">
                            <label class="label md:w-1/3 w-full md:text-base text-sm">Email</label>
                            <input class="form-control md:w-2/3 w-full" type="email" id="email" name="email">
                        </div>
                        <div class="flex mt-1">
                            <div class="md:w-1/3"></div>
                            <div class="md:w-2/3 w-full">
                                <span class="text-gray-500 text-xs" id="msg_email">{{ __('')}}</span>
                            </div>
                        </div><div class="space-y-2 md:flex items-center justify-between mt-2">
                            <label class="label md:w-1/3 w-full md:text-base text-sm">Password</label>
                            <input class="form-control md:w-2/3 w-full" type="password" id="password" name="password">
                        </div>
                        <div class="flex mt-1">
                            <div class="md:w-1/3"></div>
                            <div class="md:w-2/3 w-full">
                                <span class="text-gray-500 text-xs" id="msg_password">{{ __('')}}</span>
                            </div>
                        </div><div class="space-y-2 md:flex items-center justify-between mt-2">
                            <label class="label md:w-1/3 w-full md:text-base text-sm">Ulangi Password</label>
                            <input class="form-control md:w-2/3 w-full" type="password" id="confirm_password" name="confirm_password">
                        </div>
                        <div class="flex mt-1">
                            <div class="md:w-1/3"></div>
                            <div class="md:w-2/3 w-full">
                                <span class="text-gray-500 text-xs" id="msg_confirm_password">{{ __('')}}</span>
                            </div>
                        </div>

                        <div class="flex items-center justify-between pt-5 pb-5">
                            <span class="text-gray-500 md:text-sm text-xs">
                            Dengan mendaftar, Saya menyetujui Seluruh <span class="text-red-600">Syarat & Ketentuan</span>  yang berlaku pada website Pandi.
                            </span>
                        </div>
                        <div class="flex items-center justify-between">
                            <button type="submit" class="btn btn-default w-full">Daftar</button>
                        </div>

                    </div>
                </form>
                <div class=" justify-center text-gray-500 text-xs pb-10">
                    <p class="text-center">© 2021 PANDI - PENGELOLA NAMA DOMAIN INTERNET INDONESIA. All rights reserved</p>
                </div>
            </div>
        </div>
    </body>
</html>
