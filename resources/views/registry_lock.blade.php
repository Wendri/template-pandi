@include('layouts.header')

<style>
    .toggle-checkbox:checked {
        @apply: right-0 border-green-400;
        right: 0;
        border-color: #05976c;
    }
    .toggle-checkbox:checked + .toggle-label {
        @apply: bg-green-400;
        background-color: #05976c;
    }
</style>
<div class="banner flex items-center md:px-32 px-5">
    <a href="/domain" class="text-red-200 text-sm mr-5 flex items-center">
        <i class="ri-arrow-left-line mr-2"></i>Domain
    </a>
    <h3 class="banner-label">{{ __('Domainku.id')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="md:flex md:px-32 px-5 pt-14 items-start">
        <div class="md:w-1/5 w-full mr-5">
            <h3 class="text-lg text-gray-600 font-bold">Action</h3>
            <p class="text-md text-gray-400">Pilih Menu</p>
            <a href="/info" class="flex items-center bg-red-500 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Info
            </a>
            <a href="/registry_lock" class="flex items-center bg-red-600 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Registry Lock
            </a>
            <a href="/layanan" class="flex items-center bg-red-500 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Layanan
            </a>
        </div>
        <div class="md:w-4/5 w-full md:mt-0 mt-5">
            <h3 class="text-lg text-gray-600 font-bold">Registry Lock</h3>
            <p class="text-md text-gray-400">Kelola status penguncian Domain Anda</p>
            <div class="w-full flex items-center bg-green-200 px-3 py-2 my-2 rounded-md text-green-600 font-semibold text-base">
                REGISTRY LOCK STATUS
                <div class="relative inline-block w-12 ml-5 align-middle select-none transition duration-200 ease-in">
                    <input type="checkbox" name="toggle" id="toggle" class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-2 appearance-none cursor-pointer"/>
                    <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>
                </div>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')

