@include('layouts.header')

<style>
    li>ul                 { transform: translatex(100%) scale(0) }
    li:hover>ul           { transform: translatex(101%) scale(1) }
    li > button svg       { transform: rotate(-90deg) }
    li:hover > button svg { transform: rotate(-270deg) }
    .group:hover .group-hover\:scale-100 { transform: scale(1) }
    .group:hover .group-hover\:-rotate-180 { transform: rotate(180deg) }
    .scale-0 { transform: scale(0) }
    .min-w-32 { min-width: 8rem }
</style>

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Domain')}}</h3>
</div>

<main class="py-4 mt-10 z-0">
    <div class="lg:flex lg:flex-wrap items-center justify-between md:px-32 px-5">
        <div id='recipients' class="p-8 mt-6 md:mt-0 rounded shadow bg-white w-full">
            <div class="md:overflow-hidden overflow-x-scroll">
                <table id="example" class="stripe hover text-left" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                    <thead class="bg-gray-200">
                        <tr>
                            <th data-priority="1">NAMA DOMAIN</th>
                            <th data-priority="2">STATUS LOCK</th>
                            <th data-priority="3">TANGGAL EXPIRED</th>
                            <th data-priority="4" class="text-center">ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>domainku.web.id</td>
                            <td>
                                <p class="flex items-center text-green-400">
                                    <i class="ri-lock-line mr-3"></i>LOCKED
                                </p>
                            </td>
                            <td>Jan, 20 2021</td>
                            <td class="text-center">
                                <div class="group inline-block text-left">
                                    <button class="outline-none focus:outline-none border px-3 py-1 bg-white rounded-sm flex items-center min-w-32">
                                    <span class="pr-1 font-semibold flex-1 flex items-center"><i class="ri-lock-unlock-line mr-3"></i>Unlock</span>
                                    <span>
                                        <svg class="fill-current h-4 w-4 transform group-hover:-rotate-180
                                        transition duration-150 ease-in-out"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 20">
                                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                        </svg>
                                    </span>
                                    </button>
                                    <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute transition duration-150 ease-in-out origin-top min-w-32">
                                    <li class="rounded-sm px-3 py-1 hover:bg-gray-100 flex items-center cursor-pointer">
                                        <a href="/">
                                            <i class="ri-shopping-cart-2-line mr-3"></i>Order
                                        </a>
                                    </li>
                                    <li class="rounded-sm px-3 py-1 hover:bg-gray-100 flex items-center cursor-pointer">
                                        <a href="/info">
                                            <i class="ri-eye-line mr-3"></i>Info
                                        </a>
                                    </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>domainku.web</td>
                            <td>
                                <p class="flex items-center text-red-400">
                                    <i class="ri-shield-keyhole-line mr-3"></i>EXPIRED
                                </p>
                            </td>
                            <td>Jan, 20 2021</td>
                            <td class="text-center">
                                <div class="group inline-block text-left">
                                    <button class="outline-none focus:outline-none border px-3 py-1 bg-white rounded-sm flex items-center min-w-32">
                                    <span class="pr-1 font-semibold flex-1 flex items-center"><i class="ri-refresh-line mr-3"></i>Renew</span>
                                    <span>
                                        <svg class="fill-current h-4 w-4 transform group-hover:-rotate-180
                                        transition duration-150 ease-in-out"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 20 20">
                                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                        </svg>
                                    </span>
                                    </button>
                                    <ul class="bg-white border rounded-sm transform scale-0 group-hover:scale-100 absolute transition duration-150 ease-in-out origin-top min-w-32">
                                        <li class="rounded-sm px-3 py-1 hover:bg-gray-100 flex items-center cursor-pointer">
                                            <a href="/">
                                                <i class="ri-shopping-cart-2-line mr-3"></i>Order
                                            </a>
                                        </li>
                                        <li class="rounded-sm px-3 py-1 hover:bg-gray-100 flex items-center cursor-pointer">
                                            <a href="/info">
                                                <i class="ri-eye-line mr-3"></i>Info
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')

<script>
    $(document).ready(function() {

        var table = $('#example').DataTable( {
                responsive: true
            } )
            .columns.adjust()
            .responsive.recalc();
    } );
</script>

