@include('layouts.header')

<div class="banner flex items-center md:px-32 px-5">
    <a href="/domain" class="text-red-200 text-sm mr-5 flex items-center">
        <i class="ri-arrow-left-line mr-2"></i>Domain
    </a>
    <h3 class="banner-label">{{ __('Domainku.id')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="md:flex md:px-32 px-5 pt-14 items-start">
        <div class="md:w-1/5 w-full mr-5">
            <h3 class="text-lg text-gray-600 font-bold">Action</h3>
            <p class="text-md text-gray-400">Pilih Menu</p>
            <a href="/info" class="flex items-center bg-red-500 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Info
            </a>
            <a href="/registry_lock" class="flex items-center bg-red-500 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Registry Lock
            </a>
            <a href="/layanan" class="flex items-center bg-red-600 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Layanan
            </a>
        </div>
        <div class="md:w-4/5 w-full md:mt-0 mt-5">
            <h3 class="text-lg text-gray-600 font-bold">Layanan</h3>
            <p class="text-md text-gray-400">Informasi tentang domain Anda</p>
            <div class="md:flex items-start">
                <div class="md:w-3/5 w-full">
                    <div class="md:flex items-center py-2">
                        <div class="h-auto w-full rounded-xl border-4 border-red-600 p-5 md:mr-5">
                            <div class="flex items-center justify-between mb-7">
                                <h3 class="font-bold text-red-600 md:text-xl text-lg">domainku.id</h3>
                                <div>
                                    <span class="inline-block rounded-md text-white bg-green-400 md:px-5 px-2 py-1.5 md:text-sm text-xs">
                                        Kadaluarsa dalam 360 hari
                                    </span>
                                </div>
                            </div>
                            <h3 class="md:text-sm text-xs text-gray-400">
                                Tanggal Berakhir : 12 Desember 2021 (2 Bulan dari sekarang)
                            </h3>
                            <div class="flex items-center justify-between rounded mt-5 bg-gray-200 p-3">
                                <select name="yourDomain" id="yourDomain" class="form-control w-3/4 text-gray-900 mr-2 md:text-sm text-xs">
                                    <option value="" disabled selected>-- Pilihan --</option>
                                    <option value="1">1 Tahun @ Rp. 110.000-</option>
                                    <option value="2">2 Tahun @ Rp. 210.000-</option>
                                    <option value="3">3 Tahun @ Rp. 310.000-</option>
                                </select>
                                <button type="submit" class="btn btn-default w-1/4 ml-2 md:text-sm text-xs" @click="showModal1 = true">
                                    <i class="hidden md:block ri-shopping-cart-2-line mr-2"></i>
                                    Pilih
                                    <i class="hidden md:block ri-arrow-right-line ml-2"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="md:flex items-center py-2">
                        <div class="h-auto w-full rounded-xl border-4 border-red-600 p-5 mt-3 md:mr-5">
                            <h3 class="font-bold text-red-600 md:text-xl text-base">Catatan</h3>
                            <textarea name="note" id="note" cols="30" rows="3" class="form-control w-full mt-3 md:text-sm text-xs" placeholder="Tulis disini..."></textarea>
                        </div>
                    </div>
                </div>

                <div class="flex flex-col bg-red-600 p-5 rounded md:w-2/5 w-full mt-2">
                    <h3 class="text-xl text-gray-100 font-bold">{{ __('Rincian Pesanan')}} </h3>

                    <div class="my-3">
                        <h3 class="text-base text-red-200 uppercase font-bold">{{ __('Registry Lock')}} </h3>
                        <div class="flex items-start justify-between mt-2">
                            <label class="w-3/5 text-gray-100 text-sm font-semibold">
                                Registry Lock / Renewal
                                domainku.id / .id 1 tahun
                            </label>
                            <p class="w-2/5 text-gray-100 text-sm font-semibold text-right">Rp. 110.000-</p>
                        </div>
                        <hr class="text-red-200 mt-2">
                    </div>

                    <div class="my-3">
                        <h3 class="text-base text-red-200 uppercase font-bold">{{ __('Summary')}} </h3>
                        <div class="flex items-center justify-between mt-2">
                            <label class="w-3/5 text-gray-100 text-sm font-semibold">Subtotal</label>
                            <p class="w-2/5 text-gray-100 text-sm font-semibold text-right">Rp. 110.000-</p>
                        </div>
                        <div class="flex items-center justify-between mt-1">
                            <label class="w-3/5 text-gray-100 text-sm font-semibold">PPN @ 10.00%</label>
                            <p class="w-2/5 text-gray-100 text-sm font-semibold text-right">Rp. 11.000-</p>
                        </div>
                        <hr class="text-red-200 mt-2">
                    </div>

                    <div class="my-3">
                        <h3 class="text-base text-red-200 uppercase font-bold">{{ __('Total')}} </h3>
                        <div class="flex items-center justify-between mt-2">
                            <label class="w-2/3 text-gray-100 text-lg font-bold">Rp. 280.000-</label>
                            <i class="ri-shopping-cart-2-line w-1/4 text-gray-100 text-2xl text-right"></i>
                        </div>
                    </div>

                    <div class="my-5 mx-auto w-2/3">
                        <a href="/pembayaran" class="btn btn-secondary w-full border border-white p-2">
                            <i class="ri-checkbox-circle-fill text-red-600 pr-2"></i>
                            Checkout
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')

