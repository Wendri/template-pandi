@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Profil')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="grid grid-cols-1 gap-x-12 gap-y-5 md:grid-cols-2 md:px-32 px-5 pt-14 items-start">
       <div class="h-auto rounded-xl border-2 border-red-600 px-5 py-7">
        <form action="/verifikasi">
            <h3 class="text-lg font-bold pb-5 text-gray-600">Detail Informasi</h3>
            <div class="flex flex-col">
                <div class="md:flex items-center justify-between">
                    <label class="label md:w-1/3 w-full md:text-base text-sm">Nama Depan</label>
                    <input class="form-control md:w-2/3 w-full" type="text" id="first_name" name="first_name">
                </div>
                <div class="flex my-1">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3 w-full">
                        <span class="text-gray-500 text-xs" id="msg_first_name">{{ __('')}}</span>
                    </div>
                </div>

                <div class="md:flex items-center justify-between">
                    <label class="label md:w-1/3 w-full md:text-base text-sm">Nama Belakang</label>
                    <input class="form-control md:w-2/3 w-full" type="text" id="last_name" name="last_name">
                </div>
                <div class="flex my-1">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3 w-full">
                        <span class="text-gray-500 text-xs" id="msg_last_name">{{ __('')}}</span>
                    </div>
                </div>

                <div class="md:flex items-center justify-between">
                    <label class="label md:w-1/3 w-full md:text-base text-sm">Email</label>
                    <input class="form-control md:w-2/3 w-full" type="email" id="email" name="email">
                </div>
                <div class="flex my-1">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3 w-full">
                        <span class="text-gray-500 text-xs" id="msg_email">{{ __('')}}</span>
                    </div>
                </div>

                <div class="md:flex items-start justify-between">
                    <label class="label md:w-1/3 w-full md:text-base text-sm">Alamat</label>
                    <textarea name="address" id="address" cols="30" rows="3" class="form-control md:w-2/3 w-full"></textarea>
                </div>
                <div class="flex my-1">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3 w-full">
                        <span class="text-gray-500 text-xs" id="msg_address">{{ __('')}}</span>
                    </div>
                </div>

                <div class="md:flex items-center justify-between">
                    <label class="label md:w-1/3 w-full md:text-base text-sm">No.Telp /Whatsapp</label>
                    <input class="form-control md:w-2/3 w-full" type="text" id="telepon" name="telepon">
                </div>
                <div class="flex my-1">
                    <div class="md:w-1/3"></div>
                    <div class="md:w-2/3 w-full">
                        <span class="text-gray-500 text-xs" id="msg_telepon">{{ __('')}}</span>
                    </div>
                </div>

                <div class="flex justify-end mt-5">
                    <button type="submit" class="btn btn-default w-20">Simpan</button>
                </div>

            </div>
        </form>
       </div>
        <div class="flext items-center justify-center px-5 py-7">
            <img src="{{ asset('logo-login.svg') }}" class="md:w-4/5 w-full">
        </div>
    </div>
</main>

@include('layouts.footer')

