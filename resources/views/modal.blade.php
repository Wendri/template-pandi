
    <!-- Modal1 -->
    <div
        class="fixed inset-0 w-full h-full z-20 bg-black bg-opacity-50 duration-300 overflow-y-auto"
        x-show="showModal1"
        x-transition:enter="transition duration-300"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition duration-300"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        >
        <div class="relative sm:w-3/4 md:w-1/2 lg:w-2/3 mx-2 sm:mx-auto  opacity-100 min-h-screen flex justify-center items-center">
        <div
            class="relative bg-white shadow-lg rounded-md text-gray-600 z-20 "
            @click.away="showModal1 = false"
            x-show="showModal1"
            x-transition:enter="transition transform duration-300"
            x-transition:enter-start="scale-0"
            x-transition:enter-end="scale-100"
            x-transition:leave="transition transform duration-300"
            x-transition:leave-start="scale-100"
            x-transition:leave-end="scale-0"
        defer >
            <header class="flex items-center justify-between p-3 bg-red-600">
            <h2 class="text-white font-bold uppercase text-xl">{{ __('Beli Layanan') }}</h2>
            <button class="focus:outline-none p-2 text-white" @click="showModal1 = false">
                <svg class="fill-current" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                <path
                    d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"
                ></path>
                </svg>
            </button>
            </header>
            <form action="">
                <main class="px-5 mt-5 text-center">
                <label for="yourDomain" class="text-gray-900">Silahkan pilih domain Anda :</label>
                <select name="yourDomain" id="yourDomain" class="form-control w-full text-gray-900 mt-5">
                    <option value="" disabled selected>-- Pilihan --</option>
                    <option value="web.id">web.id</option>
                    <option value="web.com">web.com</option>
                    <option value="web.co.id">web.co.id</option>
                </select>
                </main>
                <footer class="flex items-center justify-end p-5">
                    <button type="button" class="btn btn-secondary w-20 mr-7" @click="showModal1 = false" >Batal</button>
                    <button type="submit" class="btn btn-default w-20" @click="showModal1 = false" >Pesan</button>
                </footer>
            </form>
        </div>
        </div>
    </div>
