@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Produk')}}</h3>
</div>

@include('modal')

<main class="py-4 mt-10 z-0">
    <div class="lg:flex lg:flex-wrap items-center justify-between md:px-32 px-5">
        <div class="py-5">
            <div class="bg-white border-4 border-red-600 rounded-xl space-y-6 overflow-hidden shadow-xl hover:shadow-2xl">
                <div class="text-center bg-gradient-to-r from-gray-300 to-white">
                    <div class="inline-block my-6 font-bold text-red-600 text-3xl">.web.id</div>
                </div>
                <h1 class="text-3xl text-center font-bold px-10 pt-5">
                Rp. 110.000
                    <span class="text-gray-500 text-xl">/ Tahun</span>
                </h1>
                <div class="p-5">
                    <button type="submit" class="btn btn-default w-full" @click="showModal1 = true">
                        <i class="ri-shopping-cart-2-line pr-2"></i>
                        Order Sekarang
                    </button>
                </div>
            </div>
        </div>
        <div class="py-5">
            <div class="bg-white border-4 border-red-600 rounded-xl space-y-6 overflow-hidden shadow-xl hover:shadow-2xl">
                <div class="text-center bg-gradient-to-r from-gray-300 to-white">
                    <div class="inline-block my-6 font-bold text-red-600 text-3xl">.web.id</div>
                </div>
                <h1 class="text-3xl text-center font-bold px-10 pt-5">
                Rp. 110.000
                    <span class="text-gray-500 text-xl">/ Tahun</span>
                </h1>
                <div class="p-5">
                    <button type="submit" class="btn btn-default w-full" @click="showModal1 = true">
                        <i class="ri-shopping-cart-2-line pr-2"></i>
                        Order Sekarang
                    </button>
                </div>
            </div>
        </div>
        <div class="py-5">
            <div class="bg-white border-4 border-red-600 rounded-xl space-y-6 overflow-hidden shadow-xl hover:shadow-2xl">
                <div class="text-center bg-gradient-to-r from-gray-300 to-white">
                    <div class="inline-block my-6 font-bold text-red-600 text-3xl">.web.id</div>
                </div>
                <h1 class="text-3xl text-center font-bold px-10 pt-5">
                Rp. 110.000
                    <span class="text-gray-500 text-xl">/ Tahun</span>
                </h1>
                <div class="p-5">
                    <button type="submit" class="btn btn-default w-full" @click="showModal1 = true">
                        <i class="ri-shopping-cart-2-line pr-2"></i>
                        Order Sekarang
                    </button>
                </div>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')
