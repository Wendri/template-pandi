@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Faq')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="grid grid-cols-1 md:px-32 px-5 pt-14 items-start">

        <div class="item p-3.5 w-full rounded border border-gray-400 hover:border-red-600 mb-3 hover:shadow-md cursor-pointer" x-data="{isOpen : false}" :class="{'border-red-600' : isOpen == true}">
            <a href="#" class="flex items-center justify-between" @click.prevent="isOpen = true">
                <h4 class="font-bold" :class="{'text-red-600' : isOpen == true}">{{ __('Mengapa PANDI tidak menerima pendaftaran nama domain baru ?') }}</h4>
                <i class="hidden md:block ri-more-2-fill text-right"></i>
            </a>
            <div x-show="isOpen" @click.away="isOpen = false" class="mt-3 rounded" :class="{'bg-gray-300 text-gray-600' : isOpen == true}">
                <p class="text-md p-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>

        <div class="item p-3.5 w-full rounded border border-gray-400 hover:border-red-600 mb-3 hover:shadow-md cursor-pointer" x-data="{isOpen : false}" :class="{'border-red-600' : isOpen == true}">
            <a href="#" class="flex items-center justify-between" @click.prevent="isOpen = true">
                <h4 class="font-bold" :class="{'text-red-600' : isOpen == true}">{{ __('Mengapa PANDI mengubah sistem pengelolaan menjadi SRS ?') }}</h4>
                <i class="hidden md:block ri-more-2-fill text-right"></i>
            </a>
            <div x-show="isOpen" @click.away="isOpen = false" class="mt-3 rounded" :class="{'bg-gray-300 text-gray-600' : isOpen == true}">
                <p class="text-md p-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>

        <div class="item p-3.5 w-full rounded border border-gray-400 hover:border-red-600 mb-3 hover:shadow-md cursor-pointer" x-data="{isOpen : false}" :class="{'border-red-600' : isOpen == true}">
            <a href="#" class="flex items-center justify-between" @click.prevent="isOpen = true">
                <h4 class="font-bold" :class="{'text-red-600' : isOpen == true}">{{ __('Jadi, apa fungsi PANDI sekarang ?') }}</h4>
                <i class="hidden md:block ri-more-2-fill text-right"></i>
            </a>
            <div x-show="isOpen" @click.away="isOpen = false" class="mt-3 rounded" :class="{'bg-gray-300 text-gray-600' : isOpen == true}">
                <p class="text-md p-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>

    </div>
</main>

@include('layouts.footer')

