@include('layouts.header')

<div class="banner flex items-center md:px-32 px-10">
    <a href="/billing" class="flex items-center text-red-200 text-sm mr-5">
        <i class="ri-arrow-left-line mr-2"></i>Billing
    </a>
    <h3 class="banner-label">{{ __('INV : #123')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="md:flex md:px-32 px-5 pt-14 items-start">
        <div class="h-auto md:w-1/2 w-full rounded-xl border-2 border-red-600 px-5 py-7 md:mt-0 mt-5 md:mr-2.5">
            <div class="flex items-center justify-between mb-7">
                <div class="md:flex items-center">
                    <div class="flex items-center">
                        <p class="text-md mr-2">INVOICE</p>
                        <p class="font-bold text-md mr-5">#123</p>
                    </div>
                    <span class="inline-block rounded-md text-white bg-pink-400 px-5 py-1.5 text-sm font-bold mr-3">Waiting for Payment</span>
                </div>
                <div>
                    <p class="text-sm text-gray-400">Invoice Date :</p>
                    <p class="text-sm text-gray-400">June, 25 2021</p>
                </div>
            </div>
            <div class="flex items-center">
                <div class="text-left w-1/2 mr-1">
                    <p class="text-base font-bold">{{ __('Pay to') }}</p>
                    <p class="text-xs text-gray-400">PT. PANDI INDONESIA</p>
                    <p class="text-xs text-gray-400">NPWP : 73.1234.1234.1234</p>
                    <p class="text-xs text-gray-400">Icon Business Park Unit LT. II BSD City Tangerang Selatan, Banten</p>
                </div>
                <div class="text-right w-1/2 ml-1">
                    <p class="text-base font-bold">{{ __('Invoiced to') }}</p>
                    <p class="text-xs text-gray-400">PT. PANDU ULUWATU</p>
                    <p class="text-xs text-gray-400">NPWP : 73.1234.1234.1234</p>
                    <p class="text-xs text-gray-400">Icon Business Park Unit LT. II BSD City Tangerang Selatan, Banten</p>
                </div>
            </div>
            <div class="mt-10">
                <h3 class="text-base font-bold">{{ __('Invoice Items')}} </h3>
                <div class="flex items-center my-3">
                    <label class="w-1/2 text-base font-semibold">
                        Description
                    </label>
                    <p class="w-1/2 text-base font-semibold text-right">Amount</p>
                </div>
                <hr>
                <div class="flex items-center my-3">
                    <div class="w-1/2">
                        <p class="text-md font-semibold text-red-600">
                            .id 1 tahun
                        </p>
                        <p class="text-xs text-gray-400 italic">
                            Domain : domainku.id (11 Agustus 2020 s/d 11 Agustus 2021)
                        </p>
                    </div>
                    <p class="w-1/2 text-md font-semibold text-right">Rp. 110.000-</p>
                </div>
                <hr>
                <div class="flex items-center text-right my-3">
                    <p class="w-2/3 text-sm font-semibold text-gray-600">
                        Subtotal
                    </p>
                    <p class="w-1/3 text-sm text-gray-500">Rp. 100.000-</p>
                </div>
                <div class="flex items-center text-right my-3">
                    <p class="w-2/3 text-sm font-semibold text-gray-600">
                        PPN
                    </p>
                    <p class="w-1/3 text-sm text-gray-500">Rp. 10.000-</p>
                </div>
                <div class="flex items-center text-right my-3 bg-gray-200 p-2">
                    <p class="w-2/3 text-sm font-semibold text-gray-600">
                        TOTAL
                    </p>
                    <p class="w-1/3 text-sm text-gray-500">Rp. 110.000-</p>
                </div>
                <div class="md:block hidden">
                    <h3 class="text-base font-bold">{{ __('Transaction')}} </h3>
                    <div class="flex items-center my-3">
                        <label class="w-2/6 text-xs text-gray-400 mr-2">Transaction date :</label>
                        <label class="w-1/6 text-xs text-gray-400 mr-2">Gateway :</label>
                        <label class="w-2/6 text-xs text-gray-400 mr-2">Transid :</label>
                        <label class="w-1/6 text-xs text-gray-400">Amount :</label>
                    </div>
                    <hr>
                    <div class="flex items-center my-3">
                        <p class="w-2/6 text-sm text-gray-500 mr-2">Friday, Jun 20 2021</p>
                        <p class="w-1/6 text-sm text-gray-500 mr-2">GOPAY</p>
                        <p class="w-2/6 text-sm text-gray-500 mr-2">ERUI8&HYBGFGT</p>
                        <p class="w-1/6 text-sm text-gray-500">Rp. 110.00-</p>
                    </div>
                </div>
                <div class="md:hidden block">
                    <h3 class="text-base font-bold">{{ __('Transaction')}} </h3>
                    <div class="flex items-center my-3">
                        <label class="text-xs text-gray-400 mr-2">Transaction date :</label>
                        <p class="text-sm text-gray-500">Friday, Jun 20 2021</p>
                    </div>
                    <hr>
                    <div class="flex items-center my-3">
                        <label class="text-xs text-gray-400 mr-2">Gateway :</label>
                        <p class="text-sm text-gray-500">GOPAY</p>
                    </div>
                    <hr>
                    <div class="flex items-center my-3">
                        <label class="text-xs text-gray-400 mr-2">Transid :</label>
                        <p class="text-sm text-gray-500">ERUI8&HYBGFGT</p>
                    </div>
                    <hr>
                    <div class="flex items-center my-3">
                        <label class="text-xs text-gray-400 mr-2">Amount :</label>
                        <p class="text-sm text-gray-500">Rp. 110.00-</p>
                    </div>
                </div>
            </div>
            <div class="grid justify-items-stretch">
                <button type="submit" class="btn btn-default w-3/5 justify-self-center mt-2">Konfirmasi Pembayaran</button>
            </div>
        </div>
        <div class="h-auto md:w-1/2 w-full rounded-xl bg-gray-100 px-5 py-7 md:my-0 my-10 md:ml-2.5">
            <h3 class="text-lg font-bold pb-3 text-gray-600">{{ __('Action') }}</h3>
            <a href="/pembayaran" class="btn btn-secondary w-full flex items-center">
                <i class="ri-download-2-line mr-3"></i>Download
            </a>
            <div class="mt-10">
                <h3 class="text-base font-bold">{{ __('Detail Konfirmasi')}} </h3>
                <div class="flex items-center my-3">
                    <label class="w-1/2 text-sm text-gray-600">Tanggal Konfirmasi</label>
                    <p class="w-1/2 text-sm text-gray-600 text-right">Jum'at, 21 Juni 2021 | 12:00</p>
                </div>
                <hr>
                <div class="flex items-center my-3">
                    <label class="w-1/2 text-sm text-gray-600">Transfer ke Bank</label>
                    <p class="w-1/2 text-sm text-gray-600 text-right">BCA Cabang BSD, 123456789</p>
                </div>
                <hr>
                <div class="flex items-center my-3">
                    <label class="w-1/2 text-sm text-gray-600">Rekening Pengirim</label>
                    <p class="w-1/2 text-sm text-gray-600 text-right">123456789</p>
                </div>
                <hr>
            </div>
            <div class="mt-10">
                <h3 class="text-base font-bold">{{ __('Bukti Upload Dokumen')}} </h3>
                <div class="flext items-center justify-center py-3">
                    <img src="{{ asset('logo-login.svg') }}" class="w-full border-2 h-64">
                </div>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')

