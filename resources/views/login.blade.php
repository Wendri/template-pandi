<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">


        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="bg-no-repeat bg-cover bg-center relative" style="background-image: url({{ asset('bg-login.jpg')}});">
            <div class="absolute md:pl-10 md:pt-5 pl-5 pl-3">
                <img src="{{ asset('logo.png') }}" class="h-20 w-30 mb-3">
            </div>
            <div class="min-h-screen md:flex md:bottom-0 md:flex-row mx-0 md:justify-between">
                <div class="flex-col flex self-center p-20 sm:max-w-5xl md:max-w-2xl  z-10">
                    <div class="self-start hidden md:flex flex-col text-white">
                        <img src="{{ asset('logo-login.svg') }}" class="w-80 mb-3">
                        <h1 class="mb-3 font-bold text-5xl">Pandi Registran Lock</h1>
                        <p class="pr-3">Kelola Registran lock domain .Id Anda dengan mudah dan Aman</p>
                    </div>
                    <span class="text-white text-xs absolute bottom-10 left-20">
                        © 2021 PANDI - PENGELOLA NAMA DOMAIN INTERNET INDONESIA. All rights reserved
                    </span>
                </div>

                <div class="flex self-center md:p-20 z-20 p-5">
                    <div class="md:p-10 p-5 bg-white mx-auto rounded-xl w-full shadow-lg md:drop-shadow">
                        <div class="mb-4">
                            <h3 class="font-bold text-3xl text-red-600">SIGN IN </h3>
                            <hr class="border-1 border-gray-300">
                        </div>
                        <form action="/beranda" method="">
                                <div class="space-y-3 md:flex items-center justify-between">
                                    <label class="label md:w-1/4 w-full pr-20">Email</label>
                                    <input class="form-control md:w-3/4 w-full" type="email" id="email" name="email">
                                </div>
                                <div class="md:flex md:mt-1">
                                    <label class="label md:w-1/4 pr-20"></label>
                                    <div class="md:w-3/4 w-full">
                                        <span class="text-gray-500 text-xs mb-1 md:mb-0" id="msg_email">{{ __('')}}</span>
                                    </div>
                                </div>

                                <div class="space-y-3 md:flex items-center justify-between">
                                    <label class="label md:w-1/4 w-full pr-20">Password</label>
                                    <input class="form-control md:w-3/4 w-full" type="password" id="password" name="password">
                                </div>
                                <div class="md:flex md:mt-1">
                                    <label class="label md:w-1/4 pr-20"></label>
                                    <div class="md:w-3/4 w-full">
                                        <span class="text-gray-500 text-xs mb-1 md:mb-0" id="msg_password">{{ __('')}}</span>
                                    </div>
                                </div>

                                <div class="md:space-y-5 flex items-center justify-between">
                                    <div class="md:w-1/4 md:pr-20"></div>
                                    <div class="md:w-3/4 w-full text-xs">
                                        <div class="flex items-center justify-between">
                                            <div class="flex items-center">
                                                <input id="remember_me" name="remember_me" type="checkbox" class="h-4 w-4 bg-red-600 focus:ring-red-400 border-gray-300 rounded">
                                                <label for="remember_me" class="ml-2 block text-gray-500 text-sm">
                                                    Remember me
                                                </label>
                                            </div>
                                            <a href="/forgot" class="md:pl-10 text-red-400 hover:text-red-600 text-right text-sm">
                                                Forgot password?
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-y-5 flex items-center justify-between">
                                    <div class="md:w-1/4 md:pr-20"></div>
                                    <div class="md:w-3/4 w-full">
                                        <div class="flex items-center justify-between">
                                            <button type="submit" class="btn btn-default w-20">Login</button>
                                            <a href="/register" class="btn btn-secondary w-20">Register</a>
                                        </div>
                                    </div>
                                </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </body>
</html>
