@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Syarat & Ketentuan')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="grid grid-cols-1 md:px-32 px-5 pt-14 items-start">
        <div class="w-full h-auto rounded-xl border-2 border-red-600 px-5 py-7 md:mt-0 mt-5">
            <p class="text-xl font-bold mb-5">{{ __('Kebijakan Kami') }}</p>
            <p class="text-md text-gray-400 text-justify mb-5">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
            </p>
            <p class="text-md text-gray-400 text-justify mb-5">
                Consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum optio similique perspiciatis repellat excepturi velit delectus corrupti minus tempora! Eius aspernatur commodi vel ea fuga pariatur. Provident facilis corrupti laborum.
            </p>
        </div>
    </div>
</main>

@include('layouts.footer')

