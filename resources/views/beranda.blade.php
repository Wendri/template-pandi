@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Beranda')}}</h3>
</div>

@include('modal')

<main class="py-4 z-0">
    <div class="grid grid-cols-1 gap-x-12 gap-y-5 md:grid-cols-2 md:px-32 px-5 py-14 items-center">
        <a href="#" class=" rounded-xl bg-gradient-to-r from-yellow-300 via-red-400 to-pink-400 px-3 py-5 flex flex-col undefined hover:shadow-xl">
            <div class="flex items-center justify-between">
                <div class="pt-2">
                    <i class="ri-earth-line text-white opacity-25 text-9xl"></i>
                </div>
                <div class="text-right pr-2">
                    <p class="text-white text-lg font-bold">Anda Memiliki</p>
                    <h3 class="text-white text-4xl font-bold">10</h3>
                    <p class="text-white text-lg font-semibold">Domain</p>
                </div>
            </div>
        </a>
        <a href="#" class="rounded-xl bg-gradient-to-r from-green-400 via-yellow-500 to-red-600 px-3 py-5 flex flex-col undefined hover:shadow-xl">
            <div class="flex items-center justify-between">
                <div class="pt-2">
                    <i class="ri-bank-card-line text-white opacity-25 text-9xl"></i>
                </div>
                <div class="text-right pr-2">
                    <p class="text-white text-lg font-bold">Anda Memiliki</p>
                    <h3 class="text-white text-4xl font-bold">10</h3>
                    <p class="text-white text-lg font-semibold">Tagihan</p>
                </div>
            </div>
        </a>
    </div>

    <h3 class="text-2xl text-gray-500 font-bold uppercase text-center md:py-10 py-5">{{ __('Produk Registry Lock')}}</h3>

    <div class="lg:flex lg:flex-wrap items-center justify-between md:px-32 px-10">

        <div class="py-5">
            <div class="bg-white border-4 border-red-600 rounded-xl space-y-6 overflow-hidden shadow-xl hover:shadow-2xl">
                <div class="text-center bg-gradient-to-r from-gray-300 to-white">
                    <div class="inline-block my-6 font-bold text-red-600 text-3xl">.web.id</div>
                </div>
                <h1 class="text-3xl text-center font-bold px-10 pt-5">
                Rp. 110.000
                    <span class="text-gray-500 text-xl">/ Tahun</span>
                </h1>
                <div class="p-5">
                    <button type="submit" class="btn btn-default w-full" @click="showModal1 = true">
                        <i class="ri-shopping-cart-2-line pr-2"></i>
                        Order Sekarang
                    </button>
                </div>
            </div>
        </div>

        <div class="py-5">
            <div class="bg-white border-4 border-red-600 rounded-xl space-y-6 overflow-hidden shadow-xl hover:shadow-2xl">
                <div class="text-center bg-gradient-to-r from-gray-300 to-white">
                    <div class="inline-block my-6 font-bold text-red-600 text-3xl">.web.id</div>
                </div>
                <h1 class="text-3xl text-center font-bold px-10 pt-5">
                Rp. 110.000
                    <span class="text-gray-500 text-xl">/ Tahun</span>
                </h1>
                <div class="p-5">
                    <button type="submit" class="btn btn-default w-full" @click="showModal1 = true">
                        <i class="ri-shopping-cart-2-line pr-2"></i>
                        Order Sekarang
                    </button>
                </div>
            </div>
        </div>

        <div class="py-5">
            <div class="bg-white border-4 border-red-600 rounded-xl space-y-6 overflow-hidden shadow-xl hover:shadow-2xl">
                <div class="text-center bg-gradient-to-r from-gray-300 to-white">
                    <div class="inline-block my-6 font-bold text-red-600 text-3xl">.web.id</div>
                </div>
                <h1 class="text-3xl text-center font-bold px-10 pt-5">
                Rp. 110.000
                    <span class="text-gray-500 text-xl">/ Tahun</span>
                </h1>
                <div class="p-5">
                    <button type="submit" class="btn btn-default w-full" @click="showModal1 = true">
                        <i class="ri-shopping-cart-2-line pr-2"></i>
                        Order Sekarang
                    </button>
                </div>
            </div>
        </div>


    </div>
</main>
@include('layouts.footer')

