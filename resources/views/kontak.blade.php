@include('layouts.header')

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Kontak')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="md:flex md:px-32 px-5 pt-14 items-start">
        <div class="md:w-3/5 w-full rounded-xl border-2 border-red-600 flex items-center justify-center p-3 mr-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7931.177711210961!2d106.64518627163021!3d-6.31761284663615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e4b9c856e5f7%3A0xd98c8b9d3cd44465!2sPandi%20-%20.Id%20Registry!5e0!3m2!1sen!2sid!4v1629924557826!5m2!1sen!2sid" width="640" height="640" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
       <div class="md:w-2/5 w-full rounded bg-gray-100 px-5 py-7 md:my-0 my-10">
        <form action="/verifikasi">
            <h3 class="text-lg font-bold pb-3 text-gray-600">{{ __('Hubungi Kami') }}</h3>
            <div class="flex flex-col">
                <div class="flex items-start py-2">
                    <label class="label text-sm w-1/3">Alamat</label>
                    <p class="text-sm text-gray-400 w-2/3">PT. Pandi Indonesi Icon Business Park Unit L1-L2 BSD City Tangerang Selatan, Banten</p>
                </div>
                <div class="flex items-start py-2">
                    <label class="label text-sm w-1/3">Email</label>
                    <p class="text-sm text-gray-400 w-2/3">info@pandi.com</p>
                </div>
                <div class="flex items-start py-2">
                    <label class="label text-sm w-1/3">Phone</label>
                    <p class="text-sm text-gray-400 w-2/3">022-98968465</p>
                </div>
                <div class="flex items-start py-2">
                    <label class="label text-sm w-1/3">Whatsapp</label>
                    <p class="text-sm text-gray-400 w-2/3">+62 87649087</p>
                </div>

                <div class="flex items-center py-3 my-3 bg-gray-500 rounded-full justify-center text-center">
                    <a href="#" class="rounded-full w-7 h-7 px-2 py-0.5 mx-2 bg-red-600 flex items-center justify-center">
                        <i class="fa fa-instagram text-white"></i>
                    </a>
                    <a href="#" class="rounded-full w-7 h-7 px-2 py-0.5 mx-2 bg-red-600 flex items-center justify-center">
                        <i class="fa fa-facebook text-white"></i>
                    </a>
                    <a href="#" class="rounded-full w-7 h-7 px-2 py-0.5 mx-2 bg-red-600 flex items-center justify-center">
                        <i class="fa fa-twitter text-white"></i>
                    </a>
                    <a href="#" class="rounded-full w-7 h-7 px-2 py-0.5 mx-2 bg-red-600 flex items-center justify-center">
                        <i class="fa fa-youtube text-white"></i>
                    </a>
                    <a href="#" class="rounded-full w-7 h-7 px-2 py-0.5 mx-2 bg-red-600 flex items-center justify-center">
                        <i class="fa fa-linkedin text-white"></i>
                    </a>
                </div>

                <h3 class="text-md font-bold py-3 text-gray-600">{{ __('Kirim Pesan') }}</h3>

                <div class="my-1">
                    <input class="form-control w-full" type="text" id="full_name" name="full_name" placeholder="Nama">
                    <span class="text-gray-500 text-sm pl-2 my-1" id="msg_full_name">{{ __('')}}</span>
                </div>
                <div class="my-1">
                    <input class="form-control w-full" type="text" id="telepon" name="telepon" placeholder="No.Telepon / Whatsapp">
                    <span class="text-gray-500 text-sm pl-2 my-1" id="msg_telepon">{{ __('')}}</span>
                </div>
                <div class="my-1">
                    <input class="form-control w-full" type="email" id="email" name="email" placeholder="Email">
                    <span class="text-gray-500 text-sm pl-2 my-1" id="msg_email">{{ __('')}}</span>
                </div>
                <div class="my-1">
                    <textarea name="message" id="message" cols="30" rows="3" class="form-control w-full" placeholder="Pesan Anda"></textarea>
                    <span class="text-gray-500 text-sm pl-2 my-1" id="msgFirstName">{{ __('')}}</span>
                </div>


                <div class="flex justify-end my-1">
                    <button type="submit" class="btn btn-default w-40">Kirim Pesan</button>
                </div>

            </div>
        </form>
       </div>

    </div>
</main>

@include('layouts.footer')

