@include('layouts.header')

<div class="banner flex items-center md:px-32 px-5">
    <a href="/domain" class="text-red-200 text-sm mr-5 flex items-center">
        <i class="ri-arrow-left-line mr-2"></i>Domain
    </a>
    <h3 class="banner-label">{{ __('Domainku.id')}}</h3>
</div>

<main class="pt-4 z-0">
    <div class="md:flex md:px-32 px-5 pt-14 items-start">
        <div class="md:w-1/5 w-full mr-5">
            <h3 class="text-lg text-gray-600 font-bold">Action</h3>
            <p class="text-md text-gray-400">Pilih Menu</p>
            <a href="/info" class="flex items-center bg-red-600 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Info
            </a>
            <a href="/registry_lock" class="flex items-center bg-red-500 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Registry Lock
            </a>
            <a href="/layanan" class="flex items-center bg-red-500 px-3 py-2 w-full my-2 rounded-md hover:bg-red-600 hover:shadow-lg text-gray-100 font-semibold text-base">
                Layanan
            </a>
        </div>
        <div class="md:w-4/5 w-full md:mt-0 mt-5">
            <h3 class="text-lg text-gray-600 font-bold">Info</h3>
            <p class="text-md text-gray-400">Informasi tentang domain Anda</p>
            <div class="md:flex items-center">
                <div class="bg-gradient-to-r from-yellow-300 via-red-400 to-pink-400 w-full h-80 md:my-2 p-10 rounded-lg mr-3">
                    <h3 class="text-3xl font-bold text-white text-center">domainku.id</h3>
                    <div class="flex items-center justify-center my-10">
                        <h4 class="text-lg text-white font-semibold mr-3">STATUS :</h4>
                        <button class="btn bg-green-500 border-none w-34 uppercase">Locked</button>
                    </div>
                </div>
                <div class="w-full h-80 my-2 p-3 rounded-lg border border-gray-300 md:ml-3 md:mt-0 mt-5">
                    <div class="mb-4 mt-2">
                        <p class="text-xs text-gray-400">{{ __('Tanggal Registry Lock') }}</p>
                        <div class="flex items-center my-1">
                            <p class="text-sm mr-3 text-gray-500">ICON</p>
                            <p class="text-sm text-gray-500 font-semibold">25 Agustus 2021</p>
                        </div>
                        <hr class="text-gray-500">
                    </div>
                    <div class="mb-4">
                        <p class="text-xs text-gray-400">{{ __('Tanggal Berakhir') }}</p>
                        <div class="flex items-center my-1">
                            <p class="text-sm mr-3 text-gray-500">ICON</p>
                            <p class="text-sm text-gray-500 font-semibold">25 Agustus 2021</p>
                        </div>
                        <hr class="text-gray-500">
                    </div>
                    <div class="mb-4">
                        <p class="text-xs text-gray-400">{{ __('Paket Layanan') }}</p>
                        <div class="flex items-center my-1">
                            <p class="text-sm mr-3 text-gray-500">ICON</p>
                            <p class="text-sm text-gray-500 font-semibold">Paket Langganan Tahunan</p>
                        </div>
                        <hr class="text-gray-500">
                    </div>
                    <div class="mb-4">
                        <p class="text-xs text-gray-400">{{ __('Pembayaran Terakhir') }}</p>
                        <div class="flex items-center my-1">
                            <p class="text-sm mr-3 text-gray-500">ICON</p>
                            <p class="text-sm text-red-600 font-semibold mr-3">#2342343</p>
                            <p class="text-sm text-gray-500 font-semibold">Rp. 110.000-</p>
                        </div>
                        <hr class="text-gray-500">
                    </div>
                    <div class="mb-4">
                        <p class="text-xs text-gray-400">{{ __('Metode Pembayaran') }}</p>
                        <div class="flex items-center my-1">
                            <p class="text-sm mr-3 text-gray-500">ICON</p>
                            <p class="text-sm text-gray-500 font-semibold">Credit Card</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')

