
    <!-- Modal1 -->
    <div
        class="fixed inset-0 w-full h-full z-20 bg-black bg-opacity-50 duration-300 overflow-y-auto"
        x-show="showModal1"
        x-transition:enter="transition duration-300"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition duration-300"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        >
        <div class="relative w-full mx-auto opacity-100 min-h-screen flex justify-center items-center">
        <div
            class="relative bg-white shadow-lg rounded-md text-gray-600 z-20 md:w-1/3 w-4/5"
            @click.away="showModal1 = false"
            x-show="showModal1"
            x-transition:enter="transition transform duration-300"
            x-transition:enter-start="scale-0"
            x-transition:enter-end="scale-100"
            x-transition:leave="transition transform duration-300"
            x-transition:leave-start="scale-100"
            x-transition:leave-end="scale-0"
        defer >
            <header class="flex items-center justify-between p-3 bg-red-600">
                <h2 class="text-white font-bold uppercase text-xl">{{ __('Order Summary') }}</h2>
                <button class="focus:outline-none p-2 text-white" @click="showModal1 = false">
                    <svg class="fill-current" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                    <path
                        d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"
                    ></path>
                    </svg>
                </button>
            </header>
            <div class="p-5">
                <div class="flex items-center justify-between">
                    <label for="yourDomain" class="text-gray-600 md:text-base text-sm">Ammount</label>
                    <div class="flex items-center">
                        <label for="" class="text-gray-500 text-sm mr-1">Rp.</label>
                        <p class="font-bold text-lg text-right">110.000-</p>
                    </div>
                </div>
                <div class="flex items-center bg-gray-300 p-3 my-5">
                    <label for="" class="text-gray-600 text-sm mr-1">Order Id :</label>
                    <p class="font-bold text-base text-right">UY15856</p>
                </div>
                <div class="border border-gray-400 rounded p-3 mb-10">
                    <div class="mb-2">
                        <label for="" class="text-gray-600 text-xs mr-1">Nama</label>
                        <div class="flex items-center border-b border-gray-400 pb-1">
                            <i class="ri-user-3-line mr-2"></i>
                            <p class="font-bold md:text-base text-sm text-right">Taylor Otwell </p>
                        </div>
                    </div>
                    <div class="mb-2">
                        <label for="" class="text-gray-600 text-xs mr-1">Nomor Telepon</label>
                        <div class="flex items-center border-b border-gray-400 pb-1">
                            <i class="ri-smartphone-line mr-2"></i>
                            <p class="font-bold md:text-base text-sm text-right">0212546855 </p>
                        </div>
                    </div>
                    <div class="mb-0">
                        <label for="" class="text-gray-600 text-xs mr-1">Email</label>
                        <div class="flex items-center pb-1">
                            <i class="ri-mail-line mr-2"></i>
                            <p class="font-bold md:text-base text-sm text-right">taylor.otwell@gmail.com </p>
                        </div>
                    </div>
                </div>
                <footer class="flex items-center justify-end">
                    <button type="button" class="btn btn-secondary w-1/3 md:text-base text-xs mr-5 md:h-auto h-11" @click="showModal1 = false" >Batal</button>
                    <a href="/billing" class="btn btn-default w-2/3 md:text-base text-xs text-center md:h-auto h-11" @click="showModal1 = false" >Selesaikan Pembayaran</a>
                </footer>
            </div>
        </div>
        </div>
    </div>
