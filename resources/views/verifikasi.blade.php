<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div class="bg-no-repeat bg-cover bg-center relative" style="background-image: url({{ asset('bg-flag.jpg')}});">
            <div class="min-h-screen flex flex-col justify-center max-w-lg md:m-auto mx-5 mb-5 md:space-y-8">
                <div class="flex flex-col bg-white md:p-10 p-5 rounded-lg shadow-lg">

                    <div class="flex items-center justify-between mt-5">
                        <a href="/" class="w-50 flex items-center border-2 border-red-500 bg-red-500  hover:bg-red-600  hover:border-red-600 text-gray-100 p-1  rounded-md tracking-wide text-xs  shadow-lg cursor-pointer transition ease-in duration-500"><i class="fa fa-arrow-left pr-2"></i>{{ __('Halaman Login') }}</a>
                        <img src="{{ asset('logo-color.png') }}" class="h-10">
                    </div>

                    <div class="grid auto-cols-max mt-10 md:ml-0 ml-5">
                        <h2 class="font-bold md:text-4xl text-2xl text-red-600">Pandi Registran Lock </h2>
                        <p class="font-bold text-2xl text-right">{{ __('Pendaftaran')}} </p>
                    </div>
                    <div class="text-center justify-between mb-5">
                        <i class="ri-checkbox-circle-line text-green-500" style="font-size: 100pt"></i>
                        <h3 class="font-bold md:text-2xl text-xl text-green-500">Pendaftaran Berhasil,</h3>
                        <span class="text-gray-500 md:text-lg text-sm">{{ __('Silahkan cek email Anda untuk proses') }} <a href="/konfirmasi" class="text-red-600">Verifikasi</a></span>
                    </div>
                </div>
                <div class=" justify-center text-gray-500 text-xs">
                <p class="text-center">© 2021 PANDI - PENGELOLA NAMA DOMAIN INTERNET INDONESIA. All rights reserved</p>
                </div>
            </div>
        </div>
    </body>
</html>
