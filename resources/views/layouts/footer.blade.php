<footer class="md:flax items-center justify-between relative mt-40">
    <div class="md:flex items-center justify-between absolute w-full md:px-32 px-10 py-3.5 md:top-0 top-px md:text-left text-center">
        <div class="md:flex items-center">
            <p class="text-gray-500 text-xs leading-snug">© 2021 PANDI - PENGELOLA NAMA DOMAIN INTERNET INDONESIA. <br>All rights reserved</p>
        </div>
        <div class="flex items-center justify-between md:mb-0 mb-5 text-sm">
            <a href="/profile" class="text-red-500 hover:text-red-600 px-1 font-semibold">Pandi</a>
            <p class="text-gray-500 px-1 font-semibold">|</p>
            <a href="/faq" class="text-red-500 hover:text-red-600 px-1 font-semibold">FAQ</a>
            <p class="text-gray-500 px-1 font-semibold">|</p>
            <a href="javascript:void(0);" class="text-red-500 hover:text-red-600 px-1 font-semibold">Regulasi</a>
            <p class="text-gray-500 px-1 font-semibold">|</p>
            <a href="/syarat_dan_ketentuan" class="text-red-500 hover:text-red-600 px-1 font-semibold hidden md:block">Syarat & Ketentuan</a>
            <a href="/syarat_dan_ketentuan" class="text-red-500 hover:text-red-600 px-1 font-semibold md:hidden block">S & K</a>
        </div>
        <div class="md:flax items-right hidden md:block">
            <a href="#">
                <i class="ri-question-answer-fill text-4xl text-red-600"></i>
            </a>
        </div>
    </div>
</footer>

<script src="{{ asset('js/app.js') }}"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

</body>
</html>
