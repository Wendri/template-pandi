<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css" rel="stylesheet">
        <link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body x-data="{ showModal1: false }" :class="{'overflow-y-hidden': showModal1}">
        <nav class="fixed -top-1 w-full flex items-center md:justify-between md:px-32 px-5 py-3.5 shadow-md bg-white z-10">
            <div class="md:flex">
                <div class="md:flex items-center md:mr-14">
                    <img src="{{ asset('logo-color.png') }}" class="h-10">
                </div>
                <div class="flex items-center hidden md:block md:mt-2 mt-0">
                    <a href="/beranda" class="px-3.5 hover:text-red-400 font-bold {{ Request::segment(1) === 'beranda' ? 'text-red-600' : 'text-gray-500' }}">{{ __('Beranda')}}</a>
                    <a href="/produk" class="px-3.5 hover:text-red-400 font-bold {{ Request::segment(1) === 'produk' ? 'text-red-600' : 'text-gray-500' }}">{{ __('Produk')}}</a>
                    <a href="/domain" class="px-3.5 hover:text-red-400 font-bold {{ Request::segment(1) === 'domain' ? 'text-red-600' : 'text-gray-500' }}">{{ __('Domain')}}</a>
                    <a href="/billing" class="px-3.5 hover:text-red-400 font-bold {{ Request::segment(1) === 'billing' ? 'text-red-600' : 'text-gray-500' }}">{{ __('Billing')}}</a>
                    <a href="/kontak" class="px-3.5 hover:text-red-400 font-bold {{ Request::segment(1) === 'kontak' ? 'text-red-600' : 'text-gray-500' }}">{{ __('Kontak')}}</a>
                </div>
            </div>

            <div class="flex items-center justify-self-end md:ml-0 ml-40">
                <a href="/notifikasi" class="flex md:mr-3.5">
                    <i class="ri-notification-2-fill text-xl text-gray-500 relative">
                        <i class="ri-checkbox-blank-circle-fill text-red-600 text-xs absolute top-0 right-0"></i>
                    </i>
                </a>
                <div x-data="{ dropdownOpen: false }" class="relative hidden md:block" defer>
                    <button @click="dropdownOpen = !dropdownOpen" class="btn btn-default relative z-10 block rounded-full flex items-center">
                        <svg class="flaticon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g><g>
                                <path d="M256,0c-74.439,0-135,60.561-135,135s60.561,135,135,135s135-60.561,135-135S330.439,0,256,0z"/>
                            </g></g><g><g>
                                <path d="M423.966,358.195C387.006,320.667,338.009,300,286,300h-60c-52.008,0-101.006,20.667-137.966,58.195
                                    C51.255,395.539,31,444.833,31,497c0,8.284,6.716,15,15,15h420c8.284,0,15-6.716,15-15
                                    C481,444.833,460.745,395.539,423.966,358.195z"/>
                            </g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                        </svg>
                        <span class="text-white text-sm">Pandi Wijaya</span>
                        <svg class="flaticon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 491.996 491.996" style="enable-background:new 0 0 491.996 491.996;" xml:space="preserve">
                            <g><g>
                                <path d="M484.132,124.986l-16.116-16.228c-5.072-5.068-11.82-7.86-19.032-7.86c-7.208,0-13.964,2.792-19.036,7.86l-183.84,183.848
                                    L62.056,108.554c-5.064-5.068-11.82-7.856-19.028-7.856s-13.968,2.788-19.036,7.856l-16.12,16.128
                                    c-10.496,10.488-10.496,27.572,0,38.06l219.136,219.924c5.064,5.064,11.812,8.632,19.084,8.632h0.084
                                    c7.212,0,13.96-3.572,19.024-8.632l218.932-219.328c5.072-5.064,7.856-12.016,7.864-19.224
                                    C491.996,136.902,489.204,130.046,484.132,124.986z"/>
                            </g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                        </svg>
                    </button>

                    <div x-show="dropdownOpen" @click="dropdownOpen = false" class="fixed inset-0 h-full w-full z-10"></div>

                    <div x-show="dropdownOpen" class="absolute right-0 mt-2 w-48 bg-white rounded-md shadow-xl z-20">
                        <a href="/profile" class="block px-4 py-2 text-sm text-red-600 border-b hover:bg-gray-200 font-semibold">{{ __('Profile')}}</a>
                        <a href="/domain" class="block px-4 py-2 text-sm text-red-600 border-b hover:bg-gray-200 font-semibold">{{ __('My Domain')}}</a>
                        <a href="/" class="block px-4 py-2 text-sm text-red-600 border-b hover:bg-gray-200 font-semibold">{{ __('Logout')}}</a>
                    </div>
                </div>
            </div>
            <div x-data="{ dropdownOpen: false }" class="md:hidden block ml-2">
                <button @click="dropdownOpen = !dropdownOpen" class="relative z-10 block rounded-md bg-white p-2 focus:outline-none">
                    <i class="ri-menu-line text-xl"></i>
                </button>

                <div x-show="dropdownOpen" @click="dropdownOpen = false" class="fixed inset-0 h-full w-full z-10"></div>

                <div x-show="dropdownOpen" class="absolute right-0 mt-2 py-2 w-1/2 bg-white rounded-md shadow-xl z-20">
                    <a href="/beranda" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'beranda' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Beranda')}}</a>
                    <a href="/produk" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'produk' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Produk')}}</a>
                    <a href="/domain" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'domain' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Domain')}}</a>
                    <a href="/billing" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'billing' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Billing')}}</a>
                    <a href="/kontak" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'kontak' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Kontak')}}</a>
                    <hr class="border-b border-gray-300">
                    <a href="/profile" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'profile' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Profile')}}</a>
                    <a href="/" class="block px-4 py-2 text-sm uppercase w-full hover:bg-red-600 hover:text-white font-bold {{ Request::segment(1) === 'logout' ? 'bg-red-600 text-white' : 'bg-white text-gray-400' }}">{{ __('Logout')}}</a>
                </div>
            </div>
        </nav>
