<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.2.0/fonts/remixicon.css" rel="stylesheet">
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body x-data="{ showModal1: false }" :class="{'overflow-y-hidden': showModal1}">

        <div class="bg-no-repeat bg-cover bg-center relative" style="background-image: url({{ asset('bg-flag.jpg')}});">
            <div class="min-h-screen flex flex-col justify-center max-w-lg sm:m-auto mx-5 mb-5 space-y-8">
                <form action="#">
                    <div class="flex flex-col bg-white p-5 mt-10 rounded-lg shadow-lg">

                        <div class="flex items-center justify-between mt-5">
                            <a href="/layanan" class="btn btn-default w-50 text-sm"><i class="fa fa-arrow-left pr-2"></i>{{ __('Layanan') }}</a>
                            <img src="{{ asset('logo-color.png') }}" class="h-10">
                        </div>

                        <div class="text-center font-bold justify-between mt-10">
                            <h2 class="md:text-3xl text-2xl text-red-600 uppercase">{{ __('Pembayaran')}}</h2>
                            <h3 class="md:text-xl text-md">{{ __('Pastikan pilihan Anda sudah sesuai')}} </h3>
                        </div>

                        <div class="flex flex-col bg-red-600 mt-5 p-5 rounded-lg">
                            <h3 class="md:text-2xl text-xl text-gray-100 font-bold">{{ __('Rincian Pesanan')}} </h3>

                            <div class="my-3">
                                <h3 class="md:text-lg text-base text-red-200 uppercase font-bold">{{ __('Registry Lock')}} </h3>
                                <div class="flex items-start justify-between mt-2">
                                    <label class="w-3/5 text-gray-100 text-sm font-semibold">
                                        Registry Lock / Renewal
                                        domainku.id / .id 1 tahun
                                    </label>
                                    <p class="w-2/5 text-gray-100 text-sm font-semibold text-right">Rp. 110.000-</p>
                                </div>
                                <hr class="text-red-200 mt-2">
                            </div>

                            <div class="my-3">
                                <h3 class="md:text-lg text-base text-red-200 uppercase font-bold">{{ __('Catatan')}} </h3>
                                <div class="flex items-start justify-between mt-2">
                                    <label class="w-3/5 text-gray-100 text-sm font-semibold">
                                        Mohon segera dikirim
                                    </label>
                                    <p class="w-2/5 text-gray-100 text-sm font-semibold"></p>
                                </div>
                                <hr class="text-red-200 mt-2">
                            </div>

                            <div class="my-3">
                                <h3 class="md:text-lg text-base text-red-200 uppercase font-bold">{{ __('Summary')}} </h3>
                                <div class="flex items-center justify-between mt-2">
                                    <label class="w-2/3 text-gray-100 text-sm font-semibold">Subtotal</label>
                                    <p class="w-3/5 text-gray-100 text-sm font-semibold text-right">Rp. 110.000-</p>
                                </div>
                                <div class="flex items-center justify-between mt-1">
                                    <label class="w-2/3 text-gray-100 text-sm font-semibold">PPN @ 10.00%</label>
                                    <p class="w-2/5 text-gray-100 text-sm font-semibold text-right">Rp. 11.000-</p>
                                </div>
                                <hr class="text-red-200 mt-2">
                            </div>

                            <div class="my-3">
                                <h3 class="md:text-lg text-base text-red-200 uppercase font-bold">{{ __('Total')}} </h3>
                                <div class="flex items-center justify-between mt-2">
                                    <label class="w-2/3 text-gray-100 md:text-2xl text-xl font-bold">Rp. 280.000-</label>
                                    <i class="ri-shopping-cart-2-line w-1/4 text-gray-100 text-2xl text-right"></i>
                                </div>
                            </div>

                            <div class="my-5 mx-auto w-2/3">
                                <button type="submit" class="btn btn-secondary w-full border border-white p-2" @click="showModal1 = true">
                                    <i class="ri-checkbox-circle-fill text-red-600 pr-2"></i>
                                    Bayar
                                </button>
                            </div>

                        </div>
                    </div>
                </form>
                <div class=" justify-center text-gray-500 text-xs pb-10">
                    <p class="text-center">© 2021 PANDI - PENGELOLA NAMA DOMAIN INTERNET INDONESIA. All rights reserved</p>
                </div>
            </div>
        </div>

        @include('modal-order')
        <script src="{{ asset('js/app.js') }}"></script>

    </body>
</html>
