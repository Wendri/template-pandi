@include('layouts.header')

<style>
    li>ul                 { transform: translatex(100%) scale(0) }
    li:hover>ul           { transform: translatex(101%) scale(1) }
    li > button svg       { transform: rotate(-90deg) }
    li:hover > button svg { transform: rotate(-270deg) }
    .group:hover .group-hover\:scale-100 { transform: scale(1) }
    .group:hover .group-hover\:-rotate-180 { transform: rotate(180deg) }
    .scale-0 { transform: scale(0) }
    .min-w-32 { min-width: 8rem }
</style>

<div class="banner md:text-left text-center">
    <h3 class="banner-label">{{ __('Billing')}}</h3>
</div>

<main class="py-4 mt-10 z-0">
    <div class="lg:flex lg:flex-wrap items-center justify-between md:px-32 px-5">
        <div id='recipients' class="p-8 mt-6 md:mt-0 rounded shadow bg-white w-full">
            <div class="md:overflow-hidden overflow-x-scroll">
                <table id="example" class="stripe hover text-left" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                    <thead class="bg-gray-200">
                        <tr>
                            <th data-priority="1">INVOICE</th>
                            <th data-priority="2">TANGGAL INVOICE</th>
                            <th data-priority="3" class="text-center">STATUS</th>
                            <th data-priority="4">TANGGAL EXPIRED</th>
                            <th data-priority="5" class="text-center">ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <p>
                                    <span class="text-red-600 mr-2">#123</span>Paket Langganan Bulanan
                                </p>
                            </td>
                            <td>Jan, 20 2021</td>
                            <td class="text-center">
                                <span class="inline-block rounded-full text-white bg-green-400 px-2 py-1 text-xs font-bold mr-3">PAID</span>
                            </td>
                            <td>Mar, 20 2021</td>
                            <td class="text-center">
                                <a href="/billing_paid">
                                    <i class="ri-edit-box-line"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <span class="text-red-600 mr-2">#123</span>Paket Langganan Bulanan
                                </p>
                            </td>
                            <td>Jan, 20 2021</td>
                            <td class="text-center">
                                <span class="inline-block rounded-full text-white bg-yellow-400 px-2 py-1 text-xs font-bold mr-3">UNPAID</span>
                            </td>
                            <td>Mar, 20 2021</td>
                            <td class="text-center">
                                <a href="/billing_unpaid">
                                    <i class="ri-edit-box-line"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <span class="text-red-600 mr-2">#123</span>Paket Langganan Bulanan
                                </p>
                            </td>
                            <td>Jan, 20 2021</td>
                            <td class="text-center">
                                <span class="inline-block rounded-full text-white bg-pink-400 px-2 py-1 text-xs font-bold mr-3">Waiting for Payment</span>
                            </td>
                            <td>Mar, 20 2021</td>
                            <td class="text-center">
                                <a href="/billing_waiting">
                                    <i class="ri-edit-box-line"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')

<script>
    $(document).ready(function() {

        var table = $('#example').DataTable( {
                responsive: true
            } )
            .columns.adjust()
            .responsive.recalc();
    } );
</script>

